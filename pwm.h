/*
 * pwm.h
 *
 *  Created on: 31.3.2016
 *      Author: Joonas
 */

#ifndef PWM_H_
#define PWM_H_

void TA_init(void);
void set_pwm_dc(int dc, int i);

#endif /* PWM_H_ */
